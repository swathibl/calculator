//
//  ViewController.swift
//  Calculator
//
//  Created by Swathi on 02/08/16.
//  Copyright © 2016 Swathi. All rights reserved.
//

import UIKit


enum OperationType: Int{
    case Add = 10
    case Subtract = 11
    case Multiply = 12
    case Divide = 13
}

class ViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!
    
    var operand1:  String = ""
    var operand2:  String = ""
    var result: String = ""
    var operatorType: OperationType = .Add
    var operatorArray = [ "+" , "-" , "*" , "/" ]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didTapOperand(sender: AnyObject)
    {
        let buttonTag = (sender as! UIButton).tag
        //let bTag = Double(buttonTag)
        
        var isOperator: Bool = false
        for opSymbol in operatorArray {
            if result.containsString(opSymbol) {
                isOperator = true
                }
        }
        if isOperator{
            operand2 = operand2.stringByAppendingString(String(buttonTag))
            
        }
        else {
            operand1 = operand1.stringByAppendingString(String(buttonTag))
        }
        
        result = result.stringByAppendingString(String(buttonTag))
        resultLabel.text = result
        
    }

    
    @IBAction func didTapOperator(sender: AnyObject)
    {
       operatorType =  OperationType(rawValue: (sender as! UIButton).tag)!
        
        switch operatorType
        {
        case .Add:
            result = result.stringByAppendingString("+")
            
            
        case .Subtract:
            result = result.stringByAppendingString("-")
            
        case .Multiply:
            result = result.stringByAppendingString("*")
            
        case .Divide:
            result = result.stringByAppendingString("/")
       }
        
      
        resultLabel.text = result

    }



    @IBAction func didTapResultButton(sender: AnyObject) {
        
        var opResult : Double = 0.0
        switch operatorType
        {
        case .Add:
            opResult =  (Double(operand1)! + Double(operand2)!)
        
        case .Subtract:
            opResult =  (Double(operand1)! - Double(operand2)!)
        
        case .Multiply:
            opResult =  (Double(operand1)! * Double(operand2)!)
        
        case .Divide:
            opResult = (Double(operand1)! / Double(operand2)!)
            break
        
        
        }
        
        resultLabel.text = String(opResult)
        result = ""
    }


    @IBAction func didTapClearButton(sender: AnyObject) {
        operand1 = ""
        operand2 = ""
        result = ""
        resultLabel.text = ""
    }

}

